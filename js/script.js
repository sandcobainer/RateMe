var rangeSlider = function(){
  var slider = $('.range-slider'),
      range = $('.range-slider__range'),
      value = $('.range-slider__value');
    
  slider.each(function(){

    value.each(function(){
      var value = $(this).prev().attr('value');
      $(this).html(value);
    });

    range.on('input', function(){
      $(this).next(value).html(this.value);
    });
  });
};


document.addEventListener('DOMContentLoaded', function () {
	console.log("everything loaded");
	rangeSlider(); //rating

	var chkArray=[]; //language ('.days:checked')
	$(":checkbox").on('change', function() {
   		$(this).siblings('input[type="checkbox"]').prop('checked', false);
	});
  
  $('.js-genre').select2({
    theme:"bootstrap",
    data:['Action','Adventure','Mystery'],
    tags:true,
    tokenSeparators:[','],
    placeholder:"Add genre here.." 
  }); //genre box

  $('.js-language').select2({
    theme:"classic",
    data:['English','Spanish','Hindi','German'],
    tags:true,
    tokenSeparators:[','],
    placeholder:"Add language here.." 
  }); //language box

	rangeSlider();
	document.getElementById('submitbutton').addEventListener('click', submitHandler);
	chrome.runtime.getBackgroundPage(function(eventPage) {
		getPageDetails(eventPage,onPageDetailsReceived);
	})
});
